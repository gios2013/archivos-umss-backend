DROP TABLE IF EXISTS public.archivos_umss;

CREATE TABLE IF NOT EXISTS public.archivos_umss
(
    id integer NOT NULL,
    code character varying(255) COLLATE pg_catalog."default",
    creation_date timestamp without time zone,
    modified_date timestamp without time zone,
    name character varying(255) COLLATE pg_catalog."default",
    seccion_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT archivos_umss_pkey PRIMARY KEY (id)
    )

    TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.archivos_umss
    OWNER to test1;