insert into seccion(code, description, fecha_creacion,name, num_und_documental,seccion_id, id)
values ('ARCH-UMSS', 'La Sala de archivos historicos de toda la universidad mayor de san simon', '2021-01-01','Archivos Historicos San Simon', 21-1,'aff6e021-90d9-45fe-bc0c-a8692e091620', 1000);
insert into seccion(code, description, fecha_creacion,name, num_und_documental, seccion_id, id)
values ('ReI-UMSS', 'La unidad de registros e inscripciones de la universidad para estudiantes', '2021-01-01', 'Registros e Inscripciones San Simon', 21-2,'aff6e021-90d9-45fe-bc0c-52s546666a5', 1001);

insert into category(code, description, name, category_id, seccion_id, id)
values ('TB-UMSS', 'Titulo de bachiller de los estudiantes','Bachiller','83a27c0f-c324-48c0-ad17-6f1ddc93aee0', 1000, 1000);
insert into category(code, description, name, category_id, seccion_id, id)
values ('NI-UMSS', 'Estudiantes de Nuevo Ingreso','Estudiante Nuevo','6691e7e0-7021-49ba-bb18-10ea7b3d5a9e', 1001, 1001);