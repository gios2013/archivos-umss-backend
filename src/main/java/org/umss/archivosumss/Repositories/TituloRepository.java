package org.umss.archivosumss.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.umss.archivosumss.Models.Category;
import org.umss.archivosumss.Models.Titulo;

public interface TituloRepository extends JpaRepository<Titulo, Integer> {

    @Query("select s from Titulo s where s.uuid = ?1")
    Titulo findOneByUuid(String uuid);
}
