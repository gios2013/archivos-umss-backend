package org.umss.archivosumss.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.umss.archivosumss.Models.Attachment;
import org.umss.archivosumss.Models.Titulo;

public interface TituloAttachmentRepository extends JpaRepository<Attachment, Integer> {

}
