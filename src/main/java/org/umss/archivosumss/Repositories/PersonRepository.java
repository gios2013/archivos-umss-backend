package org.umss.archivosumss.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.umss.archivosumss.Models.Person;

public interface PersonRepository extends JpaRepository<Person, Integer> {
}
