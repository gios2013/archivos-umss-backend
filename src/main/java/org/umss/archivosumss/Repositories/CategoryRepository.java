package org.umss.archivosumss.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.umss.archivosumss.Models.Category;
import org.umss.archivosumss.Models.Seccion;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

    @Query("select s from Category s where s.uuid = ?1")
    Category findOneByUuid(String uuid);
}
