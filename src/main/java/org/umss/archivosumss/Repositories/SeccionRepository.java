package org.umss.archivosumss.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.umss.archivosumss.Models.Seccion;

public interface SeccionRepository extends JpaRepository<Seccion, Integer> {

    @Query("select s from Seccion s where s.uuid = ?1")
    Seccion findOneByUuid(String uuid);
}
