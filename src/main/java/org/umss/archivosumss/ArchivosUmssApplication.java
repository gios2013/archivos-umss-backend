package org.umss.archivosumss;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.umss.archivosumss.Services.TituloAttachmentService;

@SpringBootApplication
public class ArchivosUmssApplication implements CommandLineRunner {

	@Autowired
	private TituloAttachmentService tituloAttachmentService;

	public static void main(String[] args) {
		SpringApplication.run(ArchivosUmssApplication.class, args);
	}

	@Override
	public void run(String... arg) throws Exception {
		tituloAttachmentService.init();
	}

}
