package org.umss.archivosumss.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.umss.archivosumss.DTOs.CategoryDTO;
import org.umss.archivosumss.DTOs.TituloDTO;
import org.umss.archivosumss.Services.CategoryService;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping
    public List<CategoryDTO> getAllCategories(){
        return categoryService.getAllCategories();
    }

    @PostMapping
    public CategoryDTO saveCategory(@RequestBody CategoryDTO categoryDTO){
        return categoryService.saveCategory(categoryDTO);
    }

    @GetMapping("/{categoryUuid}/titulos")
    public List<TituloDTO> getCategories(@PathVariable String categoryUuid){
        return this.categoryService.getTitulos(categoryUuid);
    }
}
