package org.umss.archivosumss.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.umss.archivosumss.DTOs.CategoryDTO;
import org.umss.archivosumss.DTOs.SeccionDTO;
import org.umss.archivosumss.Services.SeccionService;

import java.util.List;

@RestController
@RequestMapping("/seccions")
public class SeccionController {

    @Autowired
    private SeccionService seccionService;

    @GetMapping
    public List<SeccionDTO> getAllSeccions(){
        return seccionService.getAllSeccions();
    }

    @PostMapping
    public SeccionDTO saveSeccion(@RequestBody SeccionDTO seccion){
        return seccionService.saveSeccion(seccion);
    }

    @GetMapping("/{seccionUuid}")
    public SeccionDTO getByUuid(@PathVariable String seccionUuid){
        return seccionService.getSeccionByUuid(seccionUuid);
    }

    @GetMapping("/{seccionUuid}/categories")
    public List<CategoryDTO> getCategories(@PathVariable String seccionUuid){
        return this.seccionService.getCategories(seccionUuid);
    }


}
