package org.umss.archivosumss.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.umss.archivosumss.DTOs.PersonDTO;
import org.umss.archivosumss.Services.PersonService;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping
    public List<PersonDTO> getAll(){
        return personService.getAllPeople();
    }

    @PostMapping
    public PersonDTO save(@RequestBody PersonDTO personDTO){
        return personService.savePerson(personDTO);
    }
}
