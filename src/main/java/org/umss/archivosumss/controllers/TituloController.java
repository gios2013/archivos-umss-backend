package org.umss.archivosumss.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.umss.archivosumss.DTOs.TituloDTO;
import org.umss.archivosumss.DTOs.TituloFormDTO;
import org.umss.archivosumss.Services.TituloService;

import java.util.List;

@RestController
@RequestMapping("/titulo")
public class TituloController {

    @Autowired
    private TituloService tituloService;

    @GetMapping
    public List<TituloDTO> getAll(){
        return tituloService.getAll();
    }

    @PostMapping
    public TituloDTO saveTitulo(@RequestBody TituloDTO tituloDTO){
        return tituloService.saveTitulo(tituloDTO);
    }

    @PostMapping("/with-files")
    public TituloDTO saveTituloFile(TituloFormDTO tituloFormDTO){
        return tituloService.saveTitulowithFile(tituloFormDTO);
    }

}
