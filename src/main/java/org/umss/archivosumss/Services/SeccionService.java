package org.umss.archivosumss.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;
import org.umss.archivosumss.DTOs.CategoryDTO;
import org.umss.archivosumss.DTOs.CategoryMapper;
import org.umss.archivosumss.DTOs.SeccionDTO;
import org.umss.archivosumss.DTOs.SeccionMapper;
import org.umss.archivosumss.Models.Category;
import org.umss.archivosumss.Models.Seccion;
import org.umss.archivosumss.Repositories.CategoryRepository;
import org.umss.archivosumss.Repositories.SeccionRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class SeccionService {

    @Autowired
    private SeccionRepository seccionRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private SeccionMapper seccionMapper;

    @Autowired
    private CategoryMapper categoryMapper;

    public List<SeccionDTO> getAllSeccions() {
        return seccionRepository.findAll()
                .stream()
                .map(seccion -> seccionMapper.toDTO(seccion))
                .collect(Collectors.toList());
    }

    public SeccionDTO saveSeccion(SeccionDTO seccionDTO) {
        Seccion seccion = seccionMapper.getSeccion(seccionDTO);
        return seccionMapper.toDTO(seccionRepository.save(seccion));
    }

    public SeccionDTO getSeccionByUuid(String seccionUuid) {
        Seccion seccion = seccionRepository.findOneByUuid(seccionUuid);
        return seccionMapper.toDTO(seccion);
    }

    public List<CategoryDTO> getCategories(String seccionUuid) {
        Seccion example1 = new Seccion();
        example1.setUuid(seccionUuid);
        Optional<Seccion> optionalSeccion = seccionRepository.findOne(Example.of(example1));

        Seccion seccion = optionalSeccion.get();

        Category example = new Category();
        Seccion seccionExample =new Seccion();
        seccionExample.setId(seccion.getId());
        example.setSeccion(seccionExample);

        List<Category> categories = categoryRepository.findAll(Example.of(example));
        return categories
                .stream()
                .map(category -> categoryMapper.toDTO(category))
                .collect(Collectors.toList());
    }
}
