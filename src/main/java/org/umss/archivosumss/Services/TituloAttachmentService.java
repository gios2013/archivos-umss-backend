package org.umss.archivosumss.Services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;
import org.umss.archivosumss.DTOs.AttachmentDTO;
import org.umss.archivosumss.DTOs.AttachmentMapper;
import org.umss.archivosumss.Models.Attachment;
import org.umss.archivosumss.Models.Titulo;
import org.umss.archivosumss.Repositories.TituloAttachmentRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class TituloAttachmentService {

    private final Path root = Paths.get("uploads");

    @Autowired
    private TituloAttachmentRepository tituloAttachmentRepository;

    @Autowired
    private AttachmentMapper attachmentMapper;

    public void init() {
        try {
            FileSystemUtils.deleteRecursively(root.toFile());
            Files.createDirectory(root);
        } catch(IOException e) {
            throw new RuntimeException("Could not initialize folder for upload!");
        }
    }

    public AttachmentDTO saveAttachment(Titulo titulo, MultipartFile attachmentFile) {
        Path filePath = this.root.resolve(attachmentFile.getOriginalFilename());
        Attachment attachment = new Attachment();
        attachment.setTitulo(titulo);
        attachment.setObservation(attachmentFile.getOriginalFilename());
        attachment.setSize(attachmentFile.getSize());
        attachment.setPath(filePath.toString());
        titulo.setAttachment(attachment);

        tituloAttachmentRepository.save(attachment);

        try {
            Files.copy(attachmentFile.getInputStream(), filePath);
        } catch(Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }

        return attachmentMapper.toDTO(attachment);

    }
}
