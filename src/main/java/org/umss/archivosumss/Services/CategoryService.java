package org.umss.archivosumss.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;
import org.umss.archivosumss.DTOs.*;
import org.umss.archivosumss.Models.Category;
import org.umss.archivosumss.Models.Seccion;
import org.umss.archivosumss.Models.Titulo;
import org.umss.archivosumss.Repositories.CategoryRepository;
import org.umss.archivosumss.Repositories.SeccionRepository;
import org.umss.archivosumss.Repositories.TituloRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Component
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private SeccionRepository seccionRepository;

    @Autowired
    private TituloRepository tituloRepository;

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private TituloMapper tituloMapper;


    public List<CategoryDTO> getAllCategories() {
        return categoryRepository.findAll()
                .stream()
                .map(category -> categoryMapper.toDTO(category))
                .collect(Collectors.toList());
    }

    public CategoryDTO saveCategory(CategoryDTO categoryDTO) {
//        Category category =categoryMapper.getCategory(categoryDTO);
//        return categoryMapper.toDTO(categoryRepository.save(category));
        SeccionDTO seccionDTO = categoryDTO.getSeccion();


        Seccion seccion = seccionRepository.findOneByUuid(seccionDTO.getUuid());



        Category category = categoryMapper.getCategory(categoryDTO, seccion);
        categoryRepository.save(category);

        return categoryMapper.toDTO(category);
    }

    public List<TituloDTO> getTitulos(String categoryUuid) {
        Category example1 = new Category();
        example1.setUuid(categoryUuid);
        Optional<Category> optionalCategory = categoryRepository.findOne(Example.of(example1));

        Category category = optionalCategory.get();

        Titulo example = new Titulo();
        Category categoryExample =new Category();
        categoryExample.setId(category.getId());
        example.setCategory(categoryExample);

        List<Titulo> titulos = tituloRepository.findAll(Example.of(example));
        return titulos
                .stream()
                .map(titulo -> tituloMapper.toDTO(titulo))
                .collect(Collectors.toList());
    }
}
