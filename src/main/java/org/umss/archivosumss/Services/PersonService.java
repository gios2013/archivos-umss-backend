package org.umss.archivosumss.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.umss.archivosumss.DTOs.CategoryDTO;
import org.umss.archivosumss.DTOs.PersonDTO;
import org.umss.archivosumss.DTOs.PersonMapper;
import org.umss.archivosumss.DTOs.TituloDTO;
import org.umss.archivosumss.Models.Category;
import org.umss.archivosumss.Models.Person;
import org.umss.archivosumss.Models.Titulo;
import org.umss.archivosumss.Repositories.PersonRepository;
import org.umss.archivosumss.Repositories.TituloRepository;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PersonService {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private TituloRepository tituloRepository;


    public List<PersonDTO> getAllPeople() {
        return personRepository.findAll()
                .stream()
                .map(person -> personMapper.toDTO(person))
                .collect(Collectors.toList());
    }

    public PersonDTO savePerson(PersonDTO personDTO) {
//        Person person =personMapper.getPerson(personDTO);
//        return personMapper.toDTO(personRepository.save(person));
        TituloDTO tituloDTO = personDTO.getTitulo();


        Titulo titulo = tituloRepository.findOneByUuid(tituloDTO.getUuid());



        Person person = personMapper.getPerson(personDTO, titulo);
        personRepository.save(person);

        return personMapper.toDTO(person);
    }
}
