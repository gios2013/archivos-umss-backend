package org.umss.archivosumss.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.umss.archivosumss.DTOs.*;
import org.umss.archivosumss.Models.Category;
import org.umss.archivosumss.Models.Seccion;
import org.umss.archivosumss.Models.Titulo;
import org.umss.archivosumss.Repositories.CategoryRepository;
import org.umss.archivosumss.Repositories.TituloRepository;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TituloService {
    @Autowired
    private TituloRepository tituloRepository;
    @Autowired
    private TituloMapper tituloMapper;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private TituloAttachmentService tituloAttachmentService;


    public List<TituloDTO> getAll() {
        return tituloRepository.findAll()
                .stream()
                .map(titulo -> tituloMapper.toDTO(titulo))
                .collect(Collectors.toList());
    }

    public TituloDTO saveTitulo(TituloDTO tituloDTO) {
//        Titulo titulo = tituloMapper.getTitulo(tituloDTO);
//        return tituloMapper.toDTO(tituloRepository.save(titulo));
        CategoryDTO categoryDTO = tituloDTO.getCategory();


        Category category = categoryRepository.findOneByUuid(categoryDTO.getUuid());



        Titulo titulo = tituloMapper.getTitulo(tituloDTO, category);
        tituloRepository.save(titulo);

        return tituloMapper.toDTO(titulo);

    }

    public TituloDTO saveTitulowithFile(TituloFormDTO tituloFormDTO) {
        String categoryUuid = tituloFormDTO.getCategoryUuid();


        Category category = categoryRepository.findOneByUuid(categoryUuid);

        Titulo titulo = tituloMapper.getTituloFromForm(tituloFormDTO, category);
        tituloRepository.save(titulo);

        AttachmentDTO attachmentDTO = tituloAttachmentService.saveAttachment(titulo, tituloFormDTO.getAttachment());

        return tituloMapper.toDTO(titulo, attachmentDTO);
    }
}
