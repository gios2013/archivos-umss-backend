package org.umss.archivosumss.DTOs;


public class PersonDTO {

    private String uuid;
    private Integer ci;
    private String name;
    private TituloDTO titulo;

    public PersonDTO() {
    }

    public PersonDTO(String uuid, Integer ci, String name) {
        this.uuid = uuid;
        this.ci = ci;
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getCi() {
        return ci;
    }

    public void setCi(Integer ci) {
        this.ci = ci;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TituloDTO getTitulo() {
        return titulo;
    }

    public void setTitulo(TituloDTO titulo) {
        this.titulo = titulo;
    }
}
