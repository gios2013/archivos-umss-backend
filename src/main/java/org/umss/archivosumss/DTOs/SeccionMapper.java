package org.umss.archivosumss.DTOs;

import org.springframework.stereotype.Component;
import org.umss.archivosumss.Models.Seccion;

@Component
public class SeccionMapper {

    public SeccionDTO toDTO(Seccion seccion) {
        SeccionDTO seccionDTO = new SeccionDTO();
        seccionDTO.setUuid(seccion.getUuid());
        seccionDTO.setName(seccion.getName());
        seccionDTO.setCode(seccion.getCode());
        return seccionDTO;
    }

    public Seccion getSeccion(SeccionDTO seccionDTO){
        Seccion seccion = new Seccion();
        seccion.setUuid(seccionDTO.getUuid());
        seccion.setName(seccionDTO.getName());
        seccion.setCode(seccionDTO.getCode());
        return seccion;
    }


}
