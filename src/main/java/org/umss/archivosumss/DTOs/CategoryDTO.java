package org.umss.archivosumss.DTOs;

public class CategoryDTO {

    private String uuid;
    private String code;
    private String name;
    private SeccionDTO seccion;
    private String description;

    public CategoryDTO() {
    }

    public CategoryDTO(String uuid, String code, String name, String description) {
        this.uuid = uuid;
        this.code = code;
        this.name = name;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public SeccionDTO getSeccion() {
        return seccion;
    }

    public void setSeccion(SeccionDTO seccion) {
        this.seccion = seccion;
    }
}
