package org.umss.archivosumss.DTOs;

import java.util.Date;

public class TituloDTO {

    private String uuid;
    private Integer numTitulo;
    private Date dateRegister;
    private CategoryDTO category;
    private AttachmentDTO attachment;

    public TituloDTO() {
    }

    public TituloDTO(String uuid, Integer numTitulo, Date dateRegister) {
        this.uuid = uuid;
        this.numTitulo = numTitulo;
        this.dateRegister = dateRegister;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getNumTitulo() {
        return numTitulo;
    }

    public void setNumTitulo(Integer numTitulo) {
        this.numTitulo = numTitulo;
    }

    public Date getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(Date dateRegister) {
        this.dateRegister = dateRegister;
    }

    public CategoryDTO getCategory() {
        return category;
    }

    public void setCategory(CategoryDTO category) {
        this.category = category;
    }

    public AttachmentDTO getAttachment() {
        return attachment;
    }

    public void setAttachment(AttachmentDTO attachment) {
        this.attachment = attachment;
    }
}
