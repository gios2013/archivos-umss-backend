package org.umss.archivosumss.DTOs;

public class AttachmentDTO {
    private String uuid;
    private Integer numTitulo;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getNumTitulo() {
        return numTitulo;
    }

    public void setNumTitulo(Integer numTitulo) {
        this.numTitulo = numTitulo;
    }
}
