package org.umss.archivosumss.DTOs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.umss.archivosumss.Models.Category;
import org.umss.archivosumss.Models.Titulo;

@Component
public class TituloMapper {

    @Autowired
    private CategoryMapper categoryMapper;

    public TituloDTO toDTO(Titulo titulo) {
        TituloDTO tituloDTO = new TituloDTO();
        tituloDTO.setUuid(titulo.getUuid());
        tituloDTO.setNumTitulo(titulo.getNumTitulo());
        tituloDTO.setDateRegister(titulo.getDateRegister());
        tituloDTO.setCategory(categoryMapper.toDTO(titulo.getCategory()));
        return tituloDTO;
    }


    public Titulo getTitulo(TituloDTO tituloDTO, Category category) {
        Titulo titulo = new Titulo();
        titulo.setUuid(tituloDTO.getUuid());
        titulo.setNumTitulo(tituloDTO.getNumTitulo());
        titulo.setDateRegister(tituloDTO.getDateRegister());
        titulo.setCategory(category);
        return titulo;
    }

    public Titulo getTituloFromForm(TituloFormDTO tituloFormDTO, Category category) {
        Titulo titulo = new Titulo();
        titulo.setNumTitulo(tituloFormDTO.getNumTitulo());
        titulo.setDateRegister(tituloFormDTO.getDateRegister());
        titulo.setCategory(category);
        return titulo;
    }

    public TituloDTO toDTO(Titulo titulo, AttachmentDTO attachmentDTO) {
        TituloDTO tituloDTO = new TituloDTO();
        tituloDTO.setUuid(titulo.getUuid());
        tituloDTO.setNumTitulo(titulo.getNumTitulo());
        tituloDTO.setDateRegister(titulo.getDateRegister());
        tituloDTO.setCategory(categoryMapper.toDTO(titulo.getCategory()));
        tituloDTO.setAttachment(attachmentDTO);
        return tituloDTO;
    }
}
