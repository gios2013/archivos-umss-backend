package org.umss.archivosumss.DTOs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.umss.archivosumss.Models.Person;
import org.umss.archivosumss.Models.Titulo;

@Component
public class PersonMapper {

    @Autowired
    private TituloMapper tituloMapper;

    public PersonDTO toDTO(Person person) {
        PersonDTO personDTO = new PersonDTO();
        personDTO.setUuid(person.getUuid());
        personDTO.setCi(person.getCi());
        personDTO.setName(person.getName());
        personDTO.setTitulo(tituloMapper.toDTO(person.getTitulo()));
        return personDTO;
    }

    public Person getPerson(PersonDTO personDTO, Titulo titulo) {
        Person person = new Person();
        person.setUuid(personDTO.getUuid());
        person.setCi(personDTO.getCi());
        person.setName(personDTO.getName());
        person.setTitulo(titulo);
        return person;
    }
}
