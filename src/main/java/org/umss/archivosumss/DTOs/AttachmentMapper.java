package org.umss.archivosumss.DTOs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.umss.archivosumss.Models.Attachment;
import org.umss.archivosumss.Models.Category;
import org.umss.archivosumss.Models.Titulo;

@Component
public class AttachmentMapper {

    public AttachmentDTO toDTO(Attachment attachment) {
        AttachmentDTO attachmentDTO = new AttachmentDTO();
        attachmentDTO.setUuid(attachment.getUuid());
        attachmentDTO.setNumTitulo(attachment.getNumTitulo());
        return attachmentDTO;
    }


    public Attachment getTitulo(AttachmentDTO attachmentDTO, Titulo titulo) {
        Attachment attachment = new Attachment();
        attachment.setUuid(attachmentDTO.getUuid());
        attachment.setNumTitulo(attachmentDTO.getNumTitulo());
        attachment.setTitulo(titulo);
        return attachment;
    }

}
