package org.umss.archivosumss.DTOs;

import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

public class TituloFormDTO {

    private Integer numTitulo;
    private Date dateRegister;
    private String categoryUuid;
    private MultipartFile attachment;

    public Integer getNumTitulo() {
        return numTitulo;
    }

    public void setNumTitulo(Integer numTitulo) {
        this.numTitulo = numTitulo;
    }

    public Date getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(Date dateRegister) {
        this.dateRegister = dateRegister;
    }

    public String getCategoryUuid() {
        return categoryUuid;
    }

    public void setCategoryUuid(String categoryUuid) {
        this.categoryUuid = categoryUuid;
    }

    public MultipartFile getAttachment() {
        return attachment;
    }

    public void setAttachment(MultipartFile attachment) {
        this.attachment = attachment;
    }
}
