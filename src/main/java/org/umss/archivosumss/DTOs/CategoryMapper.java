package org.umss.archivosumss.DTOs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.umss.archivosumss.Models.Category;
import org.umss.archivosumss.Models.Seccion;

@Component
public class CategoryMapper {

    @Autowired
    private SeccionMapper seccionMapper;

    public CategoryDTO toDTO(Category category){
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setUuid(category.getUuid());
        categoryDTO.setName(category.getName());
        categoryDTO.setCode(category.getCode());
        categoryDTO.setDescription(category.getDescription());
        categoryDTO.setSeccion(seccionMapper.toDTO(category.getSeccion()));
        return categoryDTO;
    }

    public Category getCategory(CategoryDTO categoryDTO, Seccion seccion){
        Category category = new Category();
        category.setUuid(categoryDTO.getUuid());
        category.setName(categoryDTO.getName());
        category.setCode(categoryDTO.getCode());
        category.setDescription(categoryDTO.getDescription());
        category.setSeccion(seccion);
        return category;
    }
}
