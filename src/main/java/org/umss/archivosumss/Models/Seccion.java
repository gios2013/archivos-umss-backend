package org.umss.archivosumss.Models;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
public class Seccion {

    @Id
    @GeneratedValue
    private Integer id;
    @Column(name = "seccion_id",updatable = false, nullable = false, unique = true, length = 36)
    private String uuid;
    @Column(nullable = false, length = 500)
    private String name;
    @Column(nullable = false, length = 500)
    private String code;
    private Character serie;
    private Integer num_und_documental;
    private Integer num_ejemplares;
    private Date fecha_creacion;
    @Column(length = 500)
    private String description;
    @OneToMany(mappedBy = "seccion")
    private List<Category> categoryList;
    @CreatedDate
    @Column(updatable = false, columnDefinition = "timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP")
    private Date createdDate;
    @LastModifiedDate
    @Column(updatable = false, columnDefinition = "timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP")
    private Date modifiedDate;


    public Seccion() {
    }

    public Seccion(Integer id, String uuid, String name, String code, Character serie, Integer num_und_documental,
                   Integer num_ejemplares, Date fecha_creacion, String description, Date createdDate,
                   Date modifiedDate) {
        this.id = id;
        this.uuid = uuid;
        this.name = name;
        this.code = code;
        this.serie = serie;
        this.num_und_documental = num_und_documental;
        this.num_ejemplares = num_ejemplares;
        this.fecha_creacion = fecha_creacion;
        this.description = description;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public Character getSerie() {
        return serie;
    }

    public void setSerie(Character serie) {
        this.serie = serie;
    }

    public Integer getNum_und_documental() {
        return num_und_documental;
    }

    public void setNum_und_documental(Integer num_und_documental) {
        this.num_und_documental = num_und_documental;
    }

    public Integer getNum_ejemplares() {
        return num_ejemplares;
    }

    public void setNum_ejemplares(Integer num_ejemplares) {
        this.num_ejemplares = num_ejemplares;
    }

    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    @PrePersist
    public void initializeUuid(){
        this.setUuid(UUID.randomUUID().toString());
    }
}

