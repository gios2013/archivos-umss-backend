package org.umss.archivosumss.Models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
public class Titulo {

    @Id
    @GeneratedValue
    private Integer id;
    @Column(name = "titulo_id",updatable = false, nullable = false, unique = true, length = 36)
    private String uuid;
    private Integer numTitulo;
    private Date dateRegister;
    private String observation;
    @OneToMany(mappedBy = "titulo")
    private List<Person> personList;
    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;
    @CreatedDate
    private Date createdDate;
    @LastModifiedDate
    private Date modifiedDate;
    @OneToOne(cascade = CascadeType.REMOVE)
    private Attachment attachment;

    public Titulo() {
    }

    public Titulo(Integer id, String uuid, Integer numTitulo, Date dateRegister, String observation,
                  Date createdDate, Date modifiedDate) {
        this.id = id;
        this.uuid = uuid;
        this.numTitulo = numTitulo;
        this.dateRegister = dateRegister;
        this.observation = observation;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getNumTitulo() {
        return numTitulo;
    }

    public void setNumTitulo(Integer numTitulo) {
        this.numTitulo = numTitulo;
    }

    public Date getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(Date dateRegister) {
        this.dateRegister = dateRegister;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public List<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    @PrePersist
    public void initializeUuid(){
        this.setUuid(UUID.randomUUID().toString());
    }
}
