package org.umss.archivosumss.Models;


import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
public class Person {

    @Id
    @GeneratedValue
    private Integer id;
    @Column(name = "person_id",updatable = false, nullable = false, unique = true, length = 36)
    private String uuid;
    private Integer ci;
    private Integer passport;
    private String lastname;
    private String name;
    private Character gender;
    private Character nationality;
    @ManyToOne(fetch = FetchType.LAZY)
    private Titulo titulo;
    @CreatedDate
    private Date createdDate;
    @LastModifiedDate
    private Date modifiedDate;

    public Person() {
    }

    public Person(Integer id, String uuid, Integer ci, Integer passport, String lastname, String name, Character gender,
                  Character nationality,Date createdDate, Date modifiedDate) {
        this.id = id;
        this.uuid = uuid;
        this.ci = ci;
        this.passport = passport;
        this.lastname = lastname;
        this.name = name;
        this.gender = gender;
        this.nationality = nationality;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getCi() {
        return ci;
    }

    public void setCi(Integer ci) {
        this.ci = ci;
    }

    public Integer getPassport() {
        return passport;
    }

    public void setPassport(Integer passport) {
        this.passport = passport;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Character getGender() {
        return gender;
    }

    public void setGender(Character gender) {
        this.gender = gender;
    }

    public Character getNationality() {
        return nationality;
    }

    public void setNationality(Character nationality) {
        this.nationality = nationality;
    }

    public Titulo getTitulo() {
        return titulo;
    }

    public void setTitulo(Titulo titulo) {
        this.titulo = titulo;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @PrePersist
    public void initializeUuid(){
        this.setUuid(UUID.randomUUID().toString());
    }
}
