package org.umss.archivosumss.Models;


import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
public class Attachment {

    @Id
    @GeneratedValue
    private Integer id;
    @Column(updatable = false, nullable = false, unique = true, length = 36)
    private String uuid;
    private Integer numTitulo;
    private Date dateRegister;
    private String observation;
    private Long size;
    private String path;
    @OneToOne(mappedBy = "attachment")
    private Titulo titulo;
    @CreatedDate
    private Date createdDate;

    public Attachment() {
    }

    public Attachment(Integer id, String uuid, Integer numTitulo, Date dateRegister, String observation,
                      Date createdDate) {
        this.id = id;
        this.uuid = uuid;
        this.numTitulo = numTitulo;
        this.dateRegister = dateRegister;
        this.observation = observation;
        this.createdDate = createdDate;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getNumTitulo() {
        return numTitulo;
    }

    public void setNumTitulo(Integer numTitulo) {
        this.numTitulo = numTitulo;
    }

    public Date getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(Date dateRegister) {
        this.dateRegister = dateRegister;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Titulo getTitulo() {
        return titulo;
    }

    public void setTitulo(Titulo titulo) {
        this.titulo = titulo;
    }

    @PrePersist
    public void initializeUuid(){
        this.setUuid(UUID.randomUUID().toString());
    }
}
